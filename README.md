I've implemented collection based on concurrent collection(LinkedBlockingQueue). In my opinion this approach is much better then synchronize blocks in terms of performance.

As a test for concurrency I try to reproduce race condition for my pool. 
I've created unit test that creates 4000 threads that use my pool in the same time.
Also I've covered all other pool methods by unit test (see ResourcePoolTest)

In order to make sure my pool does not accept elements that are not origin from pool I also track all elements that pool released in separate collection. 
Also it is useful when pool needs wait for certain element for being released,