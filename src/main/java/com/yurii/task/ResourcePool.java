package com.yurii.task;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class ResourcePool<R> implements Pool<R> {

    final static Logger logger = Logger.getLogger(ResourcePool.class);

    private volatile boolean isOpen;

    private BlockingQueue<R> freeResources;
    private BlockingQueue<R> usedResources;


    public ResourcePool() {
        initializeResources();
    }

    public void open() {
        isOpen = true;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void close() {
        awaitUntilAllResourcesAreFree();
        isOpen = false;
    }

    public void closeNow() {
        isOpen = false;
    }

    public R acquire() {
        checkOpen();
        return take();
    }

    public R acquire(long timeout, TimeUnit timeUnit) {
        checkOpen();
        return poll(timeout, timeUnit);
    }

    public void release(R resource) {
        try {
            checkPoolContainsResource(resource);
            usedResources.remove(resource);
            freeResources.put(resource);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
    }

    public boolean add(R resource) {
        return freeResources.add(resource);
    }

    public boolean remove(R resource) {
        if (!isPoolContainsResource(resource)) {
            return false;
        }
        awaitUntilResourceIsFree(resource);
        return removeFromFree(resource);
    }

    public boolean removeNow(R resource) {
        if (!isPoolContainsResource(resource)) {
            return false;
        }
        return removeFromFree(resource);
    }

    private boolean isPoolContainsResource(R resource) {
        return usedResources.contains(resource) || freeResources.contains(resource);
    }

    private boolean removeFromFree(R resource) {
        return freeResources.remove(resource);
    }

    private void awaitUntilAllResourcesAreFree() {
        awaitUntil(() -> usedResources.isEmpty());
    }

    private void awaitUntilResourceIsFree(final R resource) {
        awaitUntil(() -> freeResources.contains(resource));
    }

    private synchronized void awaitUntil(Callable<Boolean> conditionEvaluator) {
        try {
            while (!conditionEvaluator.call()){
                wait();
            }
        } catch (Exception e){
            logger.error(e.getMessage());
        } finally {
            notify();
        }

    }

    private void checkPoolContainsResource(R resource) {
        if (!isPoolContainsResource(resource)) {
            throw new IllegalStateException("Pool doesn't have such resource");
        }
    }

    private void checkOpen() {
        if (!isOpen) {
            throw new IllegalStateException("Pool is not open");
        }
    }

    private R poll(long timeout, TimeUnit timeUnit) {
        try {
            return freeResources.poll(timeout, timeUnit);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    private R take() {
        try {
            R resource = freeResources.take();
            usedResources.add(resource);
            return resource;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return null;
        }
    }

    private void initializeResources() {
        freeResources = new LinkedBlockingQueue<>();
        usedResources = new LinkedBlockingQueue<>();
    }

}