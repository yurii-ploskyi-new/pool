package com.yurii.task;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;


public class ResourcePoolTest {

    @Test
    public void testOpen() {
        Pool<String> pool = new ResourcePool<>();
        assertFalse(pool.isOpen());
        pool.open();
        assertTrue(pool.isOpen());
        pool.close();
        assertFalse(pool.isOpen());
    }

    @Test
    public void testCloseWaitEndlessly() throws InterruptedException {
        ResourcePool<String> pool = new ResourcePool<>();
        assertFalse(pool.isOpen());
        pool.open();
        assertTrue(pool.isOpen());
        String resource = "some Value";
        assertTrue(pool.add(resource));
        assertNotNull(pool.acquire());
        new Thread(pool::close).start();
        assertTrue(pool.isOpen());
        pool.release(resource);
        Thread.sleep(1000);
        assertFalse(pool.isOpen());
    }

    @Test
    public void testCloseNow() throws InterruptedException {
        ResourcePool<String> pool = new ResourcePool<>();
        assertFalse(pool.isOpen());
        pool.open();
        assertTrue(pool.isOpen());
        String resource = "some Value";
        assertTrue(pool.add(resource));
        assertNotNull(pool.acquire());
        new Thread(pool::closeNow).start();
        Thread.sleep(1000);
        assertFalse(pool.isOpen());
    }

    @Test
    public void testClosedPool() {
        ResourcePool<String> pool = new ResourcePool<>();
        try {
            pool.acquire();
            fail();
        } catch (IllegalStateException e) {
            assertFalse(pool.isOpen());
        }
    }

    @Test
    public void testAcquireEmptyPool() throws InterruptedException {
        ResourcePool<String> pool = new ResourcePool<>();
        pool.open();
        AtomicReference<String> resource = new AtomicReference<>();
        new Thread(() -> resource.set(pool.acquire())).start();
        Thread.sleep(1000);
        assertNull(resource.get());
    }

    @Test
    public void testAcquireEmptyPoolWithTime() throws InterruptedException {
        ResourcePool<String> pool = new ResourcePool<>();
        pool.open();
        AtomicReference<String> resource = new AtomicReference<>();
        new Thread(() -> resource.set(pool.acquire(500, TimeUnit.MILLISECONDS))).start();
        Thread.sleep(1000);
        assertNull(resource.get());
    }

    @Test
    public void testAcquirePool() throws InterruptedException {
        ResourcePool<String> pool = new ResourcePool<>();
        pool.open();
        String resourceValue = "some Value";
        pool.add(resourceValue);
        AtomicReference<String> resource = new AtomicReference<>();
        new Thread(() -> resource.set(pool.acquire())).start();
        Thread.sleep(1000);
        assertEquals(resourceValue, resource.get());
    }

    @Test
    public void testReleaseNotExistingResource() {
        ResourcePool<String> pool = new ResourcePool<>();
        pool.open();
        String resourceValue = "some Value";
        assertTrue(pool.add(resourceValue));
        try {
            pool.release("not existing");
            fail();
        } catch (IllegalStateException e) {
        }
    }

    @Test
    public void testRelease() {
        ResourcePool<String> pool = new ResourcePool<>();
        pool.open();
        String resourceValue = "some Value";
        assertTrue(pool.add(resourceValue));
        String s = pool.acquire();
        pool.release(s);
    }

    @Test
    public void testRemove() {
        ResourcePool<String> pool = new ResourcePool<>();
        pool.open();
        String resourceValue = "some Value";
        assertTrue(pool.add(resourceValue));
        assertTrue(pool.remove(resourceValue));
        assertFalse(pool.remove("not existing"));
    }

    @Test
    public void testRemoveNow() {
        ResourcePool<String> pool = new ResourcePool<>();
        pool.open();
        String resourceValue = "some Value";
        assertTrue(pool.add(resourceValue));
        assertTrue(pool.removeNow(resourceValue));
        assertFalse(pool.removeNow("not existing"));
    }

    class TestResource {
        public double value;
        private int id = new Random().nextInt();

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TestResource that = (TestResource) o;
            return id == that.id;
        }

        @Override
        public int hashCode() {

            return Objects.hash(id);
        }
    }

    @Test
    public void testConcurrency() {
        ResourcePool<TestResource> pool = new ResourcePool<>();
        pool.open();
        pool.add(new TestResource());
        List<Thread> list = new ArrayList<>();
        for (int i = 0; i < 20000; i++) {
            list.add(new Thread(() -> {
                TestResource testResource = pool.acquire();
                pool.release(testResource);
            }));
        }

        for (int i = 0; i < 20000; i++) {
            list.add(new Thread(() -> {
                TestResource testResource = pool.acquire();
                pool.release(testResource);
            }));
        }
        list.forEach(Thread::start);
    }

}